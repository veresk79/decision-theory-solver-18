package ru.spb.beavers.modules.half_order.panels;

import cern.colt.matrix.DoubleMatrix2D;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import ru.spb.beavers.modules.half_order.graph.GraphContainer;
import ru.spb.beavers.modules.half_order.graph.matrix.operations.GraphMatrixConverter;
import ru.spb.beavers.modules.half_order.graph.matrix.operations.MatrixChecker;
import ru.spb.beavers.modules.half_order.graph.matrix.operations.MatrixOrepations;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ResolveExamplePanelBuilder {
    private Image visualizationServer;
    private Image inverserServer;
    private Image supplementServer;
    private Image doServer;
    private Image indifferenceServer;
    private ScalingControl scaler;

    private GraphContainer graphContainer;
    public ResolveExamplePanelBuilder(){
        graphContainer = GraphContainer.getInstance();
        initServers();
    }
    public void initPanel(JPanel panel){
        Box box = new Box(BoxLayout.Y_AXIS);
        box.setBorder(new LineBorder(Color.BLACK));
        box.add(getLabel("Изначальный граф"));
        box.add(getLabel(visualizationServer));
        box.add(getLabel("Переведем граф в матрицу. Изначальная матрица:"));
        box.add(getTextAreaWithMatrix(graphContainer.getGraph()));
        box.add(getLabel("Построим дополнение. Полученная матрица:"));
        box.add(getTextAreaWithMatrix(getSupplementGraph()));
        box.add(getLabel("Дополнение графа"));
        box.add(getLabel(supplementServer));
        box.add(getLabel("Построим двойственное отношение графа. Полученная матрица:"));
        box.add(getTextAreaWithMatrix(getDOGraph()));
        box.add(getLabel("Двойственное отношение графа"));
        box.add(getLabel(doServer));
        box.add(getLabel("Построим обращение графа. Полученная матрица:"));
        box.add(getTextAreaWithMatrix(getInversedGraph()));
        box.add(getLabel("Обращение графа"));
        box.add(getLabel(inverserServer));
        box.add(getLabel("Проверим, является ли данный граф отношением полупорядка."));
        DoubleMatrix2D matrix2D = GraphMatrixConverter.graphToMatrixConvert(graphContainer.getGraph());
        box.add(getTextArea(MatrixChecker.checkMatrix(matrix2D)));
        box.add(getLabel("Получим индуцируемое интервальным порядком отношение эквивалентности(безразличия). "));
        box.add(getLabel("Полученная матрица:"));
        box.add(getTextAreaWithMatrix(getIndGraph()));
        box.add(getLabel("Полученный граф:"));
        box.add(getLabel(indifferenceServer));
        panel.add(box);

    }

    private void initServers(){
        scaler = new CrossoverScalingControl();
        visualizationServer = getServer(graphContainer.getGraph());
        inverserServer = getServer(getInversedGraph());
        supplementServer = getServer(getSupplementGraph());
        doServer = getServer(getDOGraph());
        indifferenceServer = getServer(getIndGraph());
    }

    private DirectedSparseGraph<String, String>  getInversedGraph(){
        DoubleMatrix2D matrix = GraphMatrixConverter.graphToMatrixConvert(graphContainer.getGraph());
        matrix = MatrixOrepations.matrixToInverseMatrix(matrix);
        return GraphMatrixConverter.matrixToGraphConvert(matrix, graphContainer.getCorrespondenceMap());
    }

    private DirectedSparseGraph<String, String> getSupplementGraph(){
        DoubleMatrix2D matrix = GraphMatrixConverter.graphToMatrixConvert(graphContainer.getGraph());
        matrix = MatrixOrepations.matrixToSuppMatrix(matrix);
        return  GraphMatrixConverter.matrixToGraphConvert(matrix, graphContainer.getCorrespondenceMap());
    }

    private DirectedSparseGraph<String, String> getDOGraph(){
        DoubleMatrix2D matrix = GraphMatrixConverter.graphToMatrixConvert(graphContainer.getGraph());
        matrix = MatrixOrepations.matrixToSuppMatrix(matrix);
        matrix = MatrixOrepations.matrixToInverseMatrix(matrix);
        return GraphMatrixConverter.matrixToGraphConvert(matrix, graphContainer.getCorrespondenceMap());
    }

    private DirectedSparseGraph<String, String> getIndGraph(){
        DoubleMatrix2D matrix = GraphMatrixConverter.graphToMatrixConvert(graphContainer.getGraph());
        matrix = MatrixOrepations.matrixToIndifferenceMatrix(matrix);
        return GraphMatrixConverter.matrixToGraphConvert(matrix, graphContainer.getCorrespondenceMap());
    }

    private Image getServer(DirectedSparseGraph<String, String> graph){
        VisualizationImageServer server = new VisualizationImageServer(new CircleLayout(graph), new Dimension(500, 500));

        server.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        server.setVisible(true);
        scaler.scale(server, 0.8f, server.getCenter());
        return server.getImage(server.getCenter(), new Dimension(600, 600));
    }


    private TextArea getTextAreaWithMatrix(DirectedSparseGraph<String, String> graph){
        return  getTextArea(GraphMatrixConverter.graphToMatrixConvert(graph).toString());
    }


    private TextArea getTextArea(String text){
        TextArea textArea = new TextArea();
        textArea.setVisible(true);
        textArea.setEditable(false);
        textArea.setEnabled(false);
        textArea.setText(text);
        textArea.setMaximumSize(new Dimension(300, 200));
        return textArea;
    }

    private JLabel getLabel(String text){
        JLabel label = new JLabel(text);
        label.setVisible(true);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        return label;
    }

    private JLabel getLabel(Image image){
        JLabel label = new JLabel(new ImageIcon(image));
        label.setVisible(true);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        return label;
    }
}
