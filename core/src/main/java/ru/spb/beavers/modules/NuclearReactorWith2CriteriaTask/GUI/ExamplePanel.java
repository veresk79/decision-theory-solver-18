package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.ColumnGroup;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GUIElement;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GroupableTableHeader;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.LastElCoords;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.Algorithm;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.algorithm.conflictsolver.ConflictSolver;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Edge;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.Node;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IEdgeChangeHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.INodeChangeHanddler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IStepLogger;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class ExamplePanel {

    private Node root_node;
    private JTable table;
    private Algorithm algorithm;
    private String algorithm_description;
    private JLabel description_area;

    //private static Map vx_coords;
    private static Map edge_values;
    private LastElCoords last_el_coords;

    private ArrayList<Component> graphic_components;

    public ExamplePanel(JPanel panel, Node root)
    {
        graphic_components = new ArrayList<Component>();

        last_el_coords = new LastElCoords();
        panel.setLayout(null);
        panel.setSize(760, 1700);
        panel.setPreferredSize(new Dimension(750, 1700));

        createDecisionTable(panel);
        createButtonPanel(panel);
        createDecisionTextPane(panel);

        root_node = root;
        initTableFromTree(root);
        algorithm = new Algorithm(root);

        algorithm.addComponent(new IStepLogger() {
            @Override
            public void logStep(String str) {
                algorithm_description += str;
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
            }
        });
        createDecisionTree(panel, root_node);
    }

    private void createDecisionTextPane(JPanel panel)
    {
        graphic_components.add(GUIElement.setTextHeader("Описание решения", 30, 300, 20, last_el_coords, panel));
        algorithm_description = "";
        description_area = new JLabel("<html><body width=\"580\" height=\"1400\">"
                + algorithm_description
                + "</body></html>");
        JScrollPane scroll = new JScrollPane(description_area);
        scroll.setSize(600, 400);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - scroll.getWidth() / 2, last_el_coords.y);
        scroll.getVerticalScrollBar().setUnitIncrement(16);
        graphic_components.add(scroll);
    }

    private void createButtonPanel(JPanel panel) {
        JPanel jp = new JPanel();
        jp.setLayout(null);
        last_el_coords.updateValues(10, 25);
        jp.setSize(580, last_el_coords.height);
        jp.setLocation(panel.getWidth() / 2 - jp.getWidth() / 2, last_el_coords.y);
        JButton reset_btn = new JButton("Сбросить");
        reset_btn.setSize(150, 25);
        reset_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.reset();
                algorithm_description = "";
                description_area.setText("<html>"
                        + algorithm_description
                        + "</html>");
                updateDecisionTree(root_node);
            }
        });
        jp.add(reset_btn);
        JButton one_step_btn = new JButton("Сделать один шаг");
        one_step_btn.setSize(150, 25);
        one_step_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.proceedOneStep();
                updateDecisionTree(root_node);
            }
        });
        jp.add(one_step_btn);
        JButton all_steps_btn = new JButton("Дойти до конца");
        all_steps_btn.setSize(150, 25);
        all_steps_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm.solve();
                updateDecisionTree(root_node);
            }
        });
        jp.add(all_steps_btn);

        reset_btn.setLocation(jp.getWidth() / 2 - (10 * 2 + reset_btn.getWidth()
                + one_step_btn.getWidth() + all_steps_btn.getWidth()) / 2, 0);
        one_step_btn.setLocation(reset_btn.getX() + reset_btn.getWidth() + 10, 0);
        all_steps_btn.setLocation(one_step_btn.getX() + one_step_btn.getWidth() + 10, 0);

        graphic_components.add(jp);


    }

    private void createDecisionTable(JPanel panel)
    {
        graphic_components.add(GUIElement.setTextHeader("Таблица оптимальных решений", 10, 400, 20, last_el_coords, panel));
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        dm.setDataVector(new Object[][]{
                        {"1", "2", "-", "", "", "", "", ""},
                        {"1", "3.1", "-", "", "", "", "", ""},
                        {"2", "3.2", "", "", "", "", "", ""},
                        {"2", "3.3", "", "", "", "", "", ""},
                        {"3.1", "4.1", "-", "", "", "", "", ""},
                        {"3.2", "4.2", "-", "", "", "", "", ""},
                        {"3.2", "5.1", "-", "", "", "", "", ""},
                        {"3.3", "4.3", "-", "", "", "", "", ""},
                        {"3.3", "5.2", "-", "", "", "", "", ""},
                        {"4.1", "6.1", "", "", "", "", "", ""},
                        {"4.1", "6.2", "", "", "", "", "", ""},
                        {"4.2", "6.3", "", "", "", "", "", ""},
                        {"4.2", "6.4", "", "", "", "", "", ""},
                        {"5.1", "6.5", "", "", "", "", "", ""},
                        {"5.1", "6.6", "", "", "", "", "", ""},
                        {"5.1", "6.7", "", "", "", "", "", ""},
                        {"4.3", "6.8", "", "", "", "", "", ""},
                        {"4.3", "6.9", "", "", "", "", "", ""},
                        {"5.2", "6.10", "", "", "", "", "", ""},
                        {"5.2", "6.11", "", "", "", "", "", ""},
                        {"5.2", "6.12", "", "", "", "", "", ""},
                        {"6", "-", "-", "-", "-", "-", "-", "-", "-"}
                },
                new Object[]{"Узел", "Следующий\nузел", "Вероятность\nветви pj(hj)", "rj", "sj", "vj", "uj", "Оптимальное\nрешение"});
        table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMaxWidth(80);
        ColumnGroup g_1 = new ColumnGroup("Стоимость ветви");
        g_1.add(cm.getColumn(3));
        g_1.add(cm.getColumn(4));
        ColumnGroup g_2 = new ColumnGroup("Ожидаемый доход в узле");
        g_2.add(cm.getColumn(5));
        g_2.add(cm.getColumn(6));

        GroupableTableHeader header = (GroupableTableHeader)table.getTableHeader();
        header.addColumnGroup(g_1);
        header.addColumnGroup(g_2);

        table.setPreferredSize(new Dimension(700, table.getRowHeight() * 22));
        table.setSize(700, table.getRowHeight() * 22);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(700, 415);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        graphic_components.add(scroll);
    }

    private void initTableFromTree(Node node)
    {
        root_node = node;
        ArrayList<Node> node_stack = new ArrayList<Node>();
        node_stack.add(root_node);
        while (!node_stack.isEmpty())
        {
            Node current_root_node = node_stack.remove(0);
            current_root_node.addComponent(new INodeChangeHanddler() {
                @Override
                public void valueChanged(Node node) {
                    for (int i = 0; i < table.getRowCount() - 1; ++i) {
                        if (table.getModel().getValueAt(i, 1).toString().equals(node.getName())) {
                            table.getModel().setValueAt(Algorithm.round(node.getValue().getV()), i, 5);
                            table.getModel().setValueAt(Algorithm.round(node.getValue().getN()), i, 6);
                            break;
                        }
                    }
                }
            });
            if (current_root_node.getEdgeToParent() != null)
            current_root_node.getEdgeToParent().addComponent(new IEdgeChangeHandler() {
                @Override
                public void setPrefered(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((table.getModel().getValueAt(i, 0).toString()+"-"+table.getModel().getValueAt(i, 1).toString()).equals(edge.getId())) {
                            if(edge.getPreferred()) {
                                table.getModel().setValueAt(edge.getName(),i, 7);
                            } else {
                                table.getModel().setValueAt("",i, 7);
                            }
                            break;
                        }
                    }
                }

                @Override
                public void valueChanged(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((table.getModel().getValueAt(i, 0).toString()+"-"+table.getModel().getValueAt(i, 1).toString()).equals(edge.getId())) {
                            table.getModel().setValueAt(edge.getValue().getV(),i, 3);
                            table.getModel().setValueAt(edge.getValue().getN(),i, 4);
                            break;
                        }
                    }
                    updateDecisionTree(root_node);
                }

                @Override
                public void probabilityChanged(Edge edge) {
                    for (int i = 0; i < table.getRowCount()-1; ++i) {
                        if ((edge != null)&&((table.getModel().getValueAt(i, 0).toString()+"-"
                                +table.getModel().getValueAt(i, 1).toString()).equals(edge.getId())))
                        {
                            if(i != 0 || i != 1 || i != 4 || i != 5 || i != 6 || i != 7 || i != 8)
                                table.getModel().setValueAt(edge.getProbability(), i, 2);
                            break;
                        }
                    }
                }
            });
            for (Node child: current_root_node.getChilds()) {
                node_stack.add(child);
            }
            for (int i = 0; i < table.getRowCount()-1; ++i)
            {
                if ((current_root_node.getEdgeToParent() != null)&&((table.getModel().getValueAt(i, 0).toString()+"-" +table.getModel().getValueAt(i, 1).toString()).equals(current_root_node.getEdgeToParent().getId()))) {
                    if(!(i == 0 || i == 1 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8))
                        table.getModel().setValueAt(current_root_node.getEdgeToParent().getProbability(), i, 2);
                    table.getModel().setValueAt(current_root_node.getEdgeToParent().getValue().getV(),i, 3);
                    table.getModel().setValueAt(current_root_node.getEdgeToParent().getValue().getN(),i, 4);
                    break;
                }
            }
        }
    }

    public void setSolver(ConflictSolver solver) {
        algorithm.setSolver(solver);
    }

    public void updateDecisionTree(Node tree)
    {
        if (edge_values != null) {
            Vector<Node> nodes = getVectorNodeFromTree(tree);

            edge_values.clear();
            edge_values.put("N", new Point2D.Double(nodes.elementAt(0).getEdgeToParent().getValue().getV(), nodes.elementAt(0).getEdgeToParent().getValue().getN()));
            edge_values.put("T", new Point2D.Double(nodes.elementAt(1).getEdgeToParent().getValue().getV(), nodes.elementAt(1).getEdgeToParent().getValue().getN()));
            edge_values.put("C1", new Point2D.Double(nodes.elementAt(2).getEdgeToParent().getValue().getV(), nodes.elementAt(2).getEdgeToParent().getValue().getN()));
            edge_values.put("s1", new Point2D.Double(nodes.elementAt(3).getEdgeToParent().getValue().getV(), nodes.elementAt(3).getEdgeToParent().getValue().getN()));
            edge_values.put("f1", new Point2D.Double(nodes.elementAt(4).getEdgeToParent().getValue().getV(), nodes.elementAt(4).getEdgeToParent().getValue().getN()));
            edge_values.put("p", new Point2D.Double(nodes.elementAt(5).getEdgeToParent().getValue().getV(), nodes.elementAt(5).getEdgeToParent().getValue().getN()));
            edge_values.put("C2", new Point2D.Double(nodes.elementAt(6).getEdgeToParent().getValue().getV(), nodes.elementAt(6).getEdgeToParent().getValue().getN()));
            edge_values.put("s2", new Point2D.Double(nodes.elementAt(7).getEdgeToParent().getValue().getV(), nodes.elementAt(7).getEdgeToParent().getValue().getN()));
            edge_values.put("f2", new Point2D.Double(nodes.elementAt(8).getEdgeToParent().getValue().getV(), nodes.elementAt(8).getEdgeToParent().getValue().getN()));
            edge_values.put("A1", new Point2D.Double(nodes.elementAt(9).getEdgeToParent().getValue().getV(), nodes.elementAt(9).getEdgeToParent().getValue().getN()));
            edge_values.put("s3", new Point2D.Double(nodes.elementAt(10).getEdgeToParent().getValue().getV(), nodes.elementAt(10).getEdgeToParent().getValue().getN()));
            edge_values.put("m1", new Point2D.Double(nodes.elementAt(11).getEdgeToParent().getValue().getV(), nodes.elementAt(11).getEdgeToParent().getValue().getN()));
            edge_values.put("l1", new Point2D.Double(nodes.elementAt(12).getEdgeToParent().getValue().getV(), nodes.elementAt(12).getEdgeToParent().getValue().getN()));
            edge_values.put("n", new Point2D.Double(nodes.elementAt(13).getEdgeToParent().getValue().getV(), nodes.elementAt(13).getEdgeToParent().getValue().getN()));
            edge_values.put("C3", new Point2D.Double(nodes.elementAt(14).getEdgeToParent().getValue().getV(), nodes.elementAt(14).getEdgeToParent().getValue().getN()));
            edge_values.put("s4", new Point2D.Double(nodes.elementAt(15).getEdgeToParent().getValue().getV(), nodes.elementAt(15).getEdgeToParent().getValue().getN()));
            edge_values.put("f3", new Point2D.Double(nodes.elementAt(16).getEdgeToParent().getValue().getV(), nodes.elementAt(16).getEdgeToParent().getValue().getN()));
            edge_values.put("A2", new Point2D.Double(nodes.elementAt(17).getEdgeToParent().getValue().getV(), nodes.elementAt(17).getEdgeToParent().getValue().getN()));
            edge_values.put("s5", new Point2D.Double(nodes.elementAt(18).getEdgeToParent().getValue().getV(), nodes.elementAt(18).getEdgeToParent().getValue().getN()));
            edge_values.put("m2", new Point2D.Double(nodes.elementAt(19).getEdgeToParent().getValue().getV(), nodes.elementAt(19).getEdgeToParent().getValue().getN()));
            edge_values.put("l2", new Point2D.Double(nodes.elementAt(20).getEdgeToParent().getValue().getV(), nodes.elementAt(20).getEdgeToParent().getValue().getN()));
        }
    }

    private Vector<Node> getVectorNodeFromTree(Node tree)
    {
        Vector<Node> nodes = new Vector<Node>(21);
        Node node = tree.getChilds().get(1);
        nodes.add(node); // N
        nodes.add(tree.getChilds().get(0)); // T
        node = node.getChilds().get(0);
        nodes.add(node); // C1
        nodes.add(node.getChilds().get(0)); // s1
        nodes.add(node.getChilds().get(1)); // f1
        node = tree.getChilds().get(0).getChilds().get(0);
        nodes.add(node); // p
        node = node.getChilds().get(0);
        nodes.add(node); // C2
        nodes.add(node.getChilds().get(0)); // s2
        nodes.add(node.getChilds().get(1)); // f2
        node = tree.getChilds().get(0).getChilds().get(0).getChilds().get(1);
        nodes.add(node); // A1
        nodes.add(node.getChilds().get(0)); // s3
        nodes.add(node.getChilds().get(1)); // m1
        nodes.add(node.getChilds().get(2)); // l1
        node = tree.getChilds().get(0).getChilds().get(1);
        nodes.add(node); // n
        node = node.getChilds().get(0);
        nodes.add(node); // C3
        nodes.add(node.getChilds().get(0)); // s4
        nodes.add(node.getChilds().get(1)); // f3
        node = tree.getChilds().get(0).getChilds().get(1).getChilds().get(1);
        nodes.add(node); // A2
        nodes.add(node.getChilds().get(0)); // s5
        nodes.add(node.getChilds().get(1)); // m2
        nodes.add(node.getChilds().get(2)); // l2
        return nodes;
    }

    public void createDecisionTree(JPanel panel, Node tree)
    {
        Vector<Node> nodes = getVectorNodeFromTree(tree);

        edge_values = new HashMap();
        edge_values.put("N", new Point2D.Double(nodes.elementAt(0).getEdgeToParent().getValue().getV(), nodes.elementAt(0).getEdgeToParent().getValue().getN()));
        edge_values.put("T", new Point2D.Double(nodes.elementAt(1).getEdgeToParent().getValue().getV(), nodes.elementAt(1).getEdgeToParent().getValue().getN()));
        edge_values.put("C1", new Point2D.Double(nodes.elementAt(2).getEdgeToParent().getValue().getV(), nodes.elementAt(2).getEdgeToParent().getValue().getN()));
        edge_values.put("s1", new Point2D.Double(nodes.elementAt(3).getEdgeToParent().getValue().getV(), nodes.elementAt(3).getEdgeToParent().getValue().getN()));
        edge_values.put("f1", new Point2D.Double(nodes.elementAt(4).getEdgeToParent().getValue().getV(), nodes.elementAt(4).getEdgeToParent().getValue().getN()));
        edge_values.put("p", new Point2D.Double(nodes.elementAt(5).getEdgeToParent().getValue().getV(), nodes.elementAt(5).getEdgeToParent().getValue().getN()));
        edge_values.put("C2", new Point2D.Double(nodes.elementAt(6).getEdgeToParent().getValue().getV(), nodes.elementAt(6).getEdgeToParent().getValue().getN()));
        edge_values.put("s2", new Point2D.Double(nodes.elementAt(7).getEdgeToParent().getValue().getV(), nodes.elementAt(7).getEdgeToParent().getValue().getN()));
        edge_values.put("f2", new Point2D.Double(nodes.elementAt(8).getEdgeToParent().getValue().getV(), nodes.elementAt(8).getEdgeToParent().getValue().getN()));
        edge_values.put("A1", new Point2D.Double(nodes.elementAt(9).getEdgeToParent().getValue().getV(), nodes.elementAt(9).getEdgeToParent().getValue().getN()));
        edge_values.put("s3", new Point2D.Double(nodes.elementAt(10).getEdgeToParent().getValue().getV(), nodes.elementAt(10).getEdgeToParent().getValue().getN()));
        edge_values.put("m1", new Point2D.Double(nodes.elementAt(11).getEdgeToParent().getValue().getV(), nodes.elementAt(11).getEdgeToParent().getValue().getN()));
        edge_values.put("l1", new Point2D.Double(nodes.elementAt(12).getEdgeToParent().getValue().getV(), nodes.elementAt(12).getEdgeToParent().getValue().getN()));
        edge_values.put("n", new Point2D.Double(nodes.elementAt(13).getEdgeToParent().getValue().getV(), nodes.elementAt(13).getEdgeToParent().getValue().getN()));
        edge_values.put("C3", new Point2D.Double(nodes.elementAt(14).getEdgeToParent().getValue().getV(), nodes.elementAt(14).getEdgeToParent().getValue().getN()));
        edge_values.put("s4", new Point2D.Double(nodes.elementAt(15).getEdgeToParent().getValue().getV(), nodes.elementAt(15).getEdgeToParent().getValue().getN()));
        edge_values.put("f3", new Point2D.Double(nodes.elementAt(16).getEdgeToParent().getValue().getV(), nodes.elementAt(16).getEdgeToParent().getValue().getN()));
        edge_values.put("A2", new Point2D.Double(nodes.elementAt(17).getEdgeToParent().getValue().getV(), nodes.elementAt(17).getEdgeToParent().getValue().getN()));
        edge_values.put("s5", new Point2D.Double(nodes.elementAt(18).getEdgeToParent().getValue().getV(), nodes.elementAt(18).getEdgeToParent().getValue().getN()));
        edge_values.put("m2", new Point2D.Double(nodes.elementAt(19).getEdgeToParent().getValue().getV(), nodes.elementAt(19).getEdgeToParent().getValue().getN()));
        edge_values.put("l2", new Point2D.Double(nodes.elementAt(20).getEdgeToParent().getValue().getV(), nodes.elementAt(20).getEdgeToParent().getValue().getN()));

        graphic_components.add(GUIElement.setTextHeader("Дерево решений", 30, 300, 20, last_el_coords, panel));
        graphic_components.add(GUIElement.setDescriptionTree(panel, edge_values, last_el_coords, 10));
    }

    public void fillPanel(JPanel panel)
    {
        panel.setLayout(null);
        panel.setSize(760, 1700);
        panel.setPreferredSize(new Dimension(750, 1700));

        for (Component comp: graphic_components)
        {
            panel.add(comp);
        }
    }
}