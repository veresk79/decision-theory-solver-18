package ru.spb.beavers.modules;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import ru.spb.beavers.modules.hotelrooms.DistributionFunction;
import ru.spb.beavers.modules.hotelrooms.InputData;
import ru.spb.beavers.modules.hotelrooms.OutputData;
import ru.spb.beavers.modules.hotelrooms.TaskSolver;
import ru.spb.beavers.modules.hotelrooms.GUI.DescriptionPanel;
import ru.spb.beavers.modules.hotelrooms.GUI.SolutionPanel;
import ru.spb.beavers.modules.hotelrooms.GUI.Utility.LastElCoords;


/**
 * 
 * @author AntoVita
 *
 */
public class HotelRoomsModule implements ITaskModule {
	private final static String TASK_TITLE = "3.2. Задача о распределении\nгостиничных номеров";
	private final static String COUNT_OF_ROOM = "Количество номеров в гостинице";
	private final static String PRICE_FOR_TOURIST = "Цена номера для туриста";
	private final static String PRICE_FOR_BUS = "Цена номера для бизнесмена";
	private final static String TYPE_OF_DIST_FOR_BUS = "Тип распределения спроса для бизнесменов";
	private final static String TYPE_OF_DIST_FOR_TOUR = "Тип распределения спроса для туристов";
	private final static String FILE_NAME = "br6_data.txt";
	private LastElCoords last_el_coords;
	private OutputData mOutput;
	private InputData mInput;
	
	private JTextField mCountOfRooms;
	private JTextField mPriceForTourist;
	private JTextField mPriceForBus;
	private JComboBox mTypeOfDistForBus;
	private JComboBox mTypeOfDistForTour;
	
	@Override
	public String getTitle() {
		return TASK_TITLE;
	}

	@Override
	public void initDescriptionPanel(JPanel panel) {
		new DescriptionPanel(panel);
	}

	@Override
	public void initSolutionPanel(JPanel panel) {
		new SolutionPanel(panel);
	}

	@Override
	public void initInputPanel(JPanel panel) {
		
		panel.setLayout(new GridBagLayout());

		JTextField countOfRoomsLabel = new JTextField();
		mCountOfRooms = new JTextField(5);
		countOfRoomsLabel.setText(COUNT_OF_ROOM);
		countOfRoomsLabel.setEnabled(false);
		countOfRoomsLabel.setEditable(false);
		mCountOfRooms.setEditable(true);

		JTextField priceForTouristLabel = new JTextField();
		mPriceForTourist = new JTextField(5);
		priceForTouristLabel.setText(PRICE_FOR_TOURIST);
		priceForTouristLabel.setEnabled(false);
		priceForTouristLabel.setEditable(false);
		mPriceForTourist.setEditable(true);

		JTextField priceForBusLabel = new JTextField();
		mPriceForBus = new JTextField(5);
		priceForBusLabel.setText(PRICE_FOR_BUS);
		priceForBusLabel.setEnabled(false);
		priceForBusLabel.setEditable(false);
		mPriceForBus.setEditable(true);

		JTextField typeOfDistForTourLabel = new JTextField();
		mTypeOfDistForTour = new JComboBox(DistributionFunction.values());
		typeOfDistForTourLabel.setText(TYPE_OF_DIST_FOR_TOUR);
		typeOfDistForTourLabel.setEnabled(false);
		
		JTextField typeOfDistForBusLabel = new JTextField();
		mTypeOfDistForBus = new JComboBox(DistributionFunction.values());
		typeOfDistForBusLabel.setText(TYPE_OF_DIST_FOR_BUS);
		typeOfDistForBusLabel.setEnabled(false);
		typeOfDistForBusLabel.setEditable(false);
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.insets = new Insets(20, 0, 0, 0);
		c.fill = 1;
		c.gridx = 0;
		c.gridy = 0;
		panel.add(countOfRoomsLabel, c);
		c.gridx++;
		panel.add(mCountOfRooms, c);
		c.gridx++;
		panel.add(priceForTouristLabel, c);
		c.gridx++;
		panel.add(mPriceForTourist, c);
		c.gridx++;
		panel.add(priceForBusLabel, c);
		c.gridx++;
		panel.add(mPriceForBus, c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 3;
		panel.add(typeOfDistForTourLabel, c);
		c.gridx = 3;
		panel.add(mTypeOfDistForTour, c);
		c.insets.top = 0;
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 3;
		panel.add(typeOfDistForBusLabel, c);
		c.gridx = 3;
		panel.add(mTypeOfDistForBus, c);
	}

	@Override
	public void initExamplePanel(JPanel panel) {
		panel.setLayout(null);
		panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));
		/*JLabel jl = new JLabel("<html><body width=\"" + width + "\" height=\"" + height
                + "\"><center><b><i>" + header + "</i></b></center></body></html>");
        jl.setFont(new Font("Arial", Font.BOLD, 15));
        jl.setSize(width, height);
        jl.setLocation(panel.getWidth() / 2 - jl.getWidth() / 2, last_el_coords.y + last_el_coords.height + oy);
        panel.add(jl);*/
        mOutput = null;
		mInput = TaskSolver.parse(mCountOfRooms.getText(),
				mPriceForBus.getText(), mPriceForTourist.getText());
		
		
		if (mInput != null) {
			mInput.countOfVertices = 100;
			mInput.Px = (DistributionFunction)mTypeOfDistForTour.getSelectedItem();
			mInput.Py = (DistributionFunction)mTypeOfDistForBus.getSelectedItem();
			mOutput = TaskSolver.getResult(mInput);
		}
		
		if (mOutput == null) {
			JOptionPane.showMessageDialog(new JFrame(), "Входные данные введены некорректно", "Error",
			        JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		JPanel labelPanel = new JPanel(new GridBagLayout());
		labelPanel.setSize(600, 100);
		JTextField dLabel = new JTextField();
		dLabel.setText("Количество номеров, которые владелец гостинницы сдает туроператору, d: " + mOutput.d);
		dLabel.setEnabled(false);
		dLabel.setEditable(false);
		
		JTextField vLabel = new JTextField();
		vLabel.setText("Цена на номера, сдаваемые владельцем гостинницы туроператорам, Vd: " + mOutput.Vd);
		vLabel.setEnabled(false);
		vLabel.setEditable(false);
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.WEST;
		labelPanel.add(dLabel, c);
		c.gridy++;
		labelPanel.add(vLabel, c);
		
		Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("d");
        chart.getAxisY().getAxisTitle().setTitle("v");
        ITrace2D traceVf = new Trace2DSimple();
        ITrace2D traceVt = new Trace2DSimple();
        ITrace2D traceD = new Trace2DSimple();
        traceVf.setName("Vf");
        traceVf.setColor(Color.RED);
        traceVt.setName("Vt");
        traceVt.setColor(Color.BLUE);
        traceD.setName("d");
        chart.addTrace(traceVf);
        chart.addTrace(traceVt);
        chart.addTrace(traceD);

        for (int i = 0; i < mOutput.coordinates.size(); i++)
        {
        	traceVf.addPoint(mOutput.coordinates.get(i).x, mOutput.coordinates.get(i).yVf);
        	traceVt.addPoint(mOutput.coordinates.get(i).x, mOutput.coordinates.get(i).yVt);
        }
        
        traceD.addPoint(mOutput.d, chart.getAxisY().getMin());
        traceD.addPoint(mOutput.d, chart.getAxisY().getMax());
        
        ChartPanel cp = new ChartPanel(chart);
        cp.setSize(400, 400);
        cp.setLocation(32, 90);
        panel.add(labelPanel);
        panel.add(cp);
	}

	@Override
	public ActionListener getPressSaveListener()
			throws IllegalArgumentException {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				saveData();
			}
		};
	}

	@Override
	public ActionListener getPressLoadListener()
			throws IllegalArgumentException {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadData();
			}
		};
	}

	@Override
	public ActionListener getDefaultValuesListener()
			throws IllegalArgumentException {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setDefaultValues();
			}
		};
	}
	
	private void setDefaultValues() {
		mCountOfRooms.setText("100");
		mPriceForTourist.setText("10");
		mPriceForBus.setText("15");
	}
	
	private void loadData() {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(FILE_NAME));
			mCountOfRooms.setText(in.readLine());
			mPriceForTourist.setText(in.readLine());
			mPriceForBus.setText(in.readLine());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void saveData() {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(FILE_NAME);
			writer.println(mCountOfRooms.getText());
			writer.println(mPriceForTourist.getText());
			writer.println(mPriceForBus.getText());
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();
		}
	}
}
