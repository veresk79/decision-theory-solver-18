package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.gui.chart.views.ChartPanel;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GUIElement;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.GroupableTableHeader;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.GUI.Utility.LastElCoords;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SolutionPanel {
    private static Map edge_values;
    private LastElCoords last_el_coords;
    private ArrayList<Component> graphic_components;

    public SolutionPanel(JPanel panel)
    {
        graphic_components = new ArrayList<Component>();

        last_el_coords = new LastElCoords();


        panel.setLayout(null);
        panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));

        // Вычисления в случае двух атрибутов
        graphic_components.add(GUIElement.setTextHeader("Вычисления в случае двух атрибутов", 20, 300, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 400);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/calc_with_two_attr.html", last_el_coords, panel));

        // Вычисления в случае линейных компромиссов
        graphic_components.add(GUIElement.setTextHeader("Вычисления в случае линейных компромиссов", 30, 400, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 400);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/calc_linear_compromise.html", last_el_coords, panel));

        // Расчет полезностей решений.
        graphic_components.add(GUIElement.setTextHeader("Расчет полезностей решений", 30, 400, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 380);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/calc_decision_utility_1.html", last_el_coords, panel));

        // Дерево решений
        createDecisionTree(panel);
        //

        // Расчет полезностей решений. (после дерева решений)
        last_el_coords.updateValues(30, 400);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/calc_decision_utility_2.html", last_el_coords, panel));

        // «Цена» спасенной жизни.
        graphic_components.add(GUIElement.setTextHeader("\"Цена\" спасенной жизни", 30, 300, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 320);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/cost_saved_life_1.html", last_el_coords, panel));

        // График зависимости p от w
        createPlotPW(panel);

        // «Цена» спасенной жизни. (Текст после графика)
        last_el_coords.updateValues(30, 90);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/cost_saved_life_2.html", last_el_coords, panel));

        // Нелинейная полезность.
        graphic_components.add(GUIElement.setTextHeader("Нелинейная полезность", 30, 300, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 400);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/noncapital_utility.html", last_el_coords, panel));

        // Нелинейная полезность в задаче о ядерном реакторе.
        graphic_components.add(GUIElement.setTextHeader("Нелинейная полезность в задаче о ядерном реакторе", 30, 500, 20, last_el_coords, panel));
        last_el_coords.updateValues(10, 320);
        graphic_components.add(GUIElement.setHtmlText600Px("./../html/noncapital_utility_in_nuclear_reactor_task.html", last_el_coords, panel));

        // Таблица 1.4
        createCalcTable(panel);
    }

    public void fillPanel(JPanel panel)
    {
        panel.setLayout(null);
        panel.setSize(760, 4700);
        panel.setPreferredSize(new Dimension(750, 4700));

        for (Component comp: graphic_components)
        {
            panel.add(comp);
        }
    }

    public void createCalcTable(JPanel panel)
    {
        DefaultTableModel dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dm.setDataVector(new Object[][]{
                        {"1", "3", "0.852", "", "0.891", "N", "0.951", "N"},
                        {"1", "2", "0.869", "T", "0.890", "T", "0.934", ""},
                        {"2", "3", "0.911", "", "0.914", "", "0.934", ""},
                        {"2", "3", "0.807", "", "0.855", "", "0.934", ""},
                        {"3", "4", "0.853", "C", "0.891", "C", "0.951", "C"},
                        {"3", "4", "0.807", "", "0.855", "", "0.934", "C"},
                        {"3", "5", "0.911", "A", "0.914", "A", "0.923", ""},
                        {"3", "4", "0.807", "C", "0.855", "C", "0.934", "C"},
                        {"3", "5", "0.484", "", "0.509", "", "0.574", ""},
                        {"4", "6", "0.864", "", "0.901", "", "0.959", ""},
                        {"4", "6", "0.318", "", "0.396", "", "0.577", ""},
                        {"4", "6", "0.818", "", "0.866", "", "0.942", ""},
                        {"4", "6", "0.273", "", "0.344", "", "0.518", ""},
                        {"4", "6", "0.818", "", "0.866", "", "0.942", ""},
                        {"4", "6", "0.273", "", "0.344", "", "0.518", ""},
                        {"5", "6", "1.0", "", "1.0", "", "1.0", ""},
                        {"5", "6", "0.182", "", "0.237", "", "0.38", ""},
                        {"5", "6", "0", "", "0", "", "0", ""},
                        {"5", "6", "1.0", "", "1.0", "", "1.0", ""},
                        {"5", "6", "0.182", "", "0.237", "", "0.38", ""},
                        {"5", "6", "0", "", "0", "", "0", ""},
                        {"6", "-", "-", "-", "-", "-", "-", "-"}
                },
                new Object[]{"Узел", "Следующий\nузел", "Полезность\nпри β=1", "Оптимальное\nрешение",
                        "Полезность\nпри β=2", "Оптимальное\nрешение", "Полезность\nпри β=10", "Оптимальное\nрешение"});
        JTable table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        cm.getColumn(0).setMaxWidth(50);
        table.setPreferredSize(new Dimension(650,table.getRowHeight()*22));
        table.setSize(650, table.getRowHeight() * 22);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(650, 415);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(30, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        graphic_components.add(scroll);
    }

    public void createPlotPW(JPanel panel)
    {
        Chart2D chart = new Chart2D();
        chart.getAxisX().getAxisTitle().setTitle("w");
        chart.getAxisY().getAxisTitle().setTitle("p");
        ITrace2D trace = new Trace2DSimple();
        trace.setName("");
        chart.addTrace(trace);
        double p;
        int w = 0;
        trace.addPoint(0, 0);
        do {
            p = 1.0/(2.28-0.1*w);
            if (p > 0.01) {
                trace.addPoint(w, p);
            }
            w++;
        } while((p > 0.01)&&(p<=1));
        ChartPanel cp = new ChartPanel(chart);
        cp.setSize(400, 400);
        last_el_coords.updateValues(30, cp.getHeight());
        cp.setLocation(panel.getWidth() / 2 - cp.getWidth() / 2, last_el_coords.y);

        graphic_components.add(cp);
    }

    public void createDecisionTree(JPanel panel)
    {
        edge_values = new HashMap();
        edge_values.put("N", new Point2D.Double(0, 0));
        edge_values.put("T", new Point2D.Double(-1, 0));
        edge_values.put("C1", new Point2D.Double(-2, 0));
        edge_values.put("s1", new Point2D.Double(10, 0));
        edge_values.put("f1", new Point2D.Double(-2, 4));
        edge_values.put("p", new Point2D.Double(0, 0));
        edge_values.put("C2", new Point2D.Double(-2, 0));
        edge_values.put("s2", new Point2D.Double(10, 0));
        edge_values.put("f2", new Point2D.Double(-2, 4));
        edge_values.put("A1", new Point2D.Double(-4, 0));
        edge_values.put("s3", new Point2D.Double(16, 0));
        edge_values.put("m1", new Point2D.Double(-2, 1));
        edge_values.put("l1", new Point2D.Double(-6, 3));
        edge_values.put("n", new Point2D.Double(0, 0));
        edge_values.put("C3", new Point2D.Double(-2, 0));
        edge_values.put("s4", new Point2D.Double(10, 0));
        edge_values.put("f3", new Point2D.Double(-2, 4));
        edge_values.put("A2", new Point2D.Double(-4, 0));
        edge_values.put("s5", new Point2D.Double(16, 0));
        edge_values.put("m2", new Point2D.Double(-2, 1));
        edge_values.put("l2", new Point2D.Double(-6, 3));
        graphic_components.add(GUIElement.setDescriptionTree(panel, edge_values, last_el_coords, 10));
    }
}