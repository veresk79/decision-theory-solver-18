package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse;

import java.io.Serializable;

/**
 * Created by Sasha on 11.04.2015.
 */
public class datainp implements Serializable {
    private double[] pg;
    private double[] pt;
    private int[] rwin;
    private int[] rlose;
    private int[] rdraw;
    private String[] des;
    private String[] deso;
    public datainp()
    {
        pg=new double[10];
        pt=new double[10];
        rwin=new int[10];
        rlose=new int[10];
        rdraw=new int[10];
        pg[0]=0.2;
        pt[0]=0.1;
        rwin[0]=112;
        rlose[0]=106;
        rdraw[0]=109;

        pg[1]=0.6;
        pt[1]=0.2;
        rwin[1]=212;
        rlose[1]=200;
        rdraw[1]=206;

        pg[2]=0.33;
        pt[2]=0.3;
        rwin[2]=111;
        rlose[2]=109;
        rdraw[2]=110;

        pg[3]=0.7;
        pt[3]=0.5;
        rwin[3]=628;
        rlose[3]=555;
        rdraw[3]=596;

        pg[4]=0.4;
        pt[4]=0.2;
        rwin[4]=134;
        rlose[4]=122;
        rdraw[4]=130;

        pg[5]=0.8;
        pt[5]=0.2;
        rwin[5]=121;
        rlose[5]=100;
        rdraw[5]=110;

        pg[6]=0.6;
        pt[6]=0.15;
        rwin[6]=354;
        rlose[6]=222;
        rdraw[6]=288;

        pg[7]=0.6;
        pt[7]=0.3;
        rwin[7]=100;
        rlose[7]=90;
        rdraw[7]=95;


        pg[8]=0.8;
        pt[8]=0.3;
        rwin[8]=223;
        rlose[8]=200;
        rdraw[8]=220;


        pg[9]=0.4;
        pt[9]=0.3;
        rwin[9]=345;
        rlose[9]=300;
        rdraw[9]=325;
        des=new String[10];
        des[0]="-";
        des[1]="-";
        des[2]="-";
        des[3]="-";
        des[4]="-";
        des[5]="-";
        des[6]="-";
        des[7]="-";
        des[8]="-";
        des[9]="-";
        deso=new String[10];
        deso[0]="-";
        deso[1]="-";
        deso[2]="-";
        deso[3]="-";
        deso[4]="-";
        deso[5]="-";
        deso[6]="-";
        deso[7]="-";
        deso[8]="-";
        deso[9]="-";
    }

    public void ubdPg(double value, int nstr)
    {
        pg[nstr]=value;

    }
    public void ubdPt(double value, int nstr)
    {
        pt[nstr]=value;

    }
    public void ubdRwin(int value, int nstr)
    {
        rwin[nstr]=value;

    }
    public void ubdRlose(int value, int nstr)
    {
        rlose[nstr]=value;

    }
    public void ubdRdraw(int value, int nstr)
    {
        rdraw[nstr]=value;

    }

    public double getpg(int nst)
    {
        return pg[nst];
    }
    public double getpt(int nst)
    {
        return pt[nst];
    }
    public int getrwin(int nst)
    {
        return rwin[nst];
    }
    public int getrlose(int nst)
    {
        return rlose[nst];
    }
    public int getrdraw(int nst)
    {
        return rdraw[nst];
    }
    public String getrdes(int nst)
    {
        return des[nst];
    }
    public String getrdeso(int nst)
    {
        return deso[nst];
    }
    public void ubddes(int nst, String d)
    {
        des[nst]=d;
    }
    public void ubddeso(int nst, String  d)
    {
        deso[nst]=d;
    }

}
