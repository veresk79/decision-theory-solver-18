package ru.spb.beavers.modules.Task_3_1_2;

/**
 * Created by Никита on 13.04.15.
 */
public class TaskCommon {
    public static final String GLOBAL_TITLE = "3.1.2 Задача о распределении\nресурсов";
    public static final String DESC_TITLE = "<html><h2 align='center'>Неформальное описание задачи</h2></html>";
    public static final String FORMAL_TITLE = "Формальная постановка задачи";
    public static final String USEFULL_TITLE = "Расчет полезностей";
    public static final String SOLUTION_TITLE = "Решение";

    public static final String SOLUTION_TYPE = "Тип решения: ";
    public static final String SOLUTION_CLASS_1 = "Маржинальный анализ";
    public static final String SOLUTION_CLASS_2 = "Динамический анализ";
    public static final String SPROS_SIZE =  "Количество билетов со скидкой:";
    public static final String COUNT_PLACE = "Количество мест(M):                     ";
    public static final String PRICE_CLASS_1 = "Стоимость билетов без скидки: ";
    public static final String PRICE_CLASS_2 = "Стоимость билетов со скидкой: ";
    public static final String VEROYATNOST =   "Вероятность запроса:                    ";


    public static final String DIAGRAMM_NAME = "Диаграмма влияния нашей задачи";

    public static final String DESC_MESSAGE = "Пусть авиакомпания продает билеты на плановый рейс самолета. Продажа билетов начинается \n" +
            "задолго до запланированной даты вылета. При заблаговременной покупке билетов пассажиры \n" +
            "получают на билеты скидку. Непосредственно перед вылетом билеты продаются по полной \n" +
            "стоимости. Все непроданные заблаговременно со скидкой билеты поступают в продажу по полной\n" +
            "стоимости. Предполагается, что никто из пассажиров купленные на рейс билеты не сдает.\n" +
            "Доход авиакомпании определяется стоимостью проданных билетов. В случае если авиакомпания \n" +
            "оставляет на продажу по полной стоимости меньше билетов, чем наблюдается спрос, то \n" +
            "авиакомпания не получает части дохода, который определяется разницей между стоимостью\n" +
            "билетов по полной цене и билетами со скидкой и спросом на билеты по полной цене. В случае \n" +
            "если авиакомпания принимает решение продавать больше билетов по полной цене, чем на них\n" +
            "наблюдается спрос, то компания теряет доход, равный стоимости непроданных билетов. Задача\n" +
            "сводится к выбору оптимального соотношения между количеством билетов, продаваемых  по полной\n" +
            "цене, и количеством билетов, продаваемых со скидкой.";



    public static final String PIC_AIRPLANE ="task_3_1/airplane2.jpg";


    public static final String PIC_FORMAL = "task_3_1/task_3_1_2_diagramm.png";
    public static final String PIC_USEFULL = "task_3_1/task_3_1_2_useful.png";
    public static final String PIC_SOLUTION = "task_3_1/task_3_1_2_solution.png";
    public static final String PIC_RESOLVE_1 = "task_3_1/task_3_1_2_solve_1.png";
    public static final String PIC_RESOLVE_2 = "task_3_1/task_3_1_2_solve_2.png";
    //public static final String PIC_RESOLVE_USE_FORMULA = "images/task_3_1_1_formulaUse.png";
    //public static final String PIC_RESOLVE_USE_FORMULA2 = "images/task_3_1_1_formulaUse2.png";
}
